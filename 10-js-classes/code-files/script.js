// WHY USE A CLASS?

const person = {
  name: "Mubaraq",
  email: "mub@gmail.com",
  phoneNumber: "+1234567890",
  status: "student",
}

const person2 = Object.assign({}, person);
person2.name = "";

// CLASS DECLARATION

// class Person {
//   // ...
// }


// CLASS EXPRESSION

const Animal = class {
  // ...
}


// CONSTRUCTORS AND INSTANCE PROPS

// class Person {
//   constructor(name, email, phone, status) {
//     this.name = name;
//     this.email = email;
//     this.phoneNumber = phone;
//     this.status = status;
//   }
// }

// const p = new Person("Mubaraq", "mub@gmail.com", "12456789", "studying");

// console.log(p)

// const q = new Person("Esat", "yaman@esatco", "4567890", "working")

// console.log(q)


// INSTANCE METHODS

class Person {
  constructor(name, email, phone, status) {
    this.name = name;
    this.email = email;
    this.phoneNumber = phone;
    this.status = status;
  }

  greet() {
    console.log(`Hello from ${this.name}`);
  }
}

const p = new Person("Mubaraq", "mub@gmail.com", "12456789", "studying");

p.name = "Wahab"
p.greet();

const q = new Person("Esat", "yaman@esatco", "4567890", "working")

console.log(q.phoneNumber)

q.greet()