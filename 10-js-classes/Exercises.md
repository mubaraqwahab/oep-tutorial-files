# Exercises for 10: JS Classes

I encourage you to try these out after watching the video.
In case something's not clear, or you get stuck, feel free to ask me.
All the best!

## Exercise 1

Let's practice creating classes!

Create an `HTMLElement` class, and declare two properties within it: `tagName` and `content`. Also, define a `render` method.

You should be able to use the class like so:

```js
const p = new HTMLElement("p", "Hey! This is a paragraph");
p.render()
// OUTPUT
// <p>Hey! This is a paragraph</p>

const button = new HTMLElement("button", "Click here!");
button.render();
// OUTPUT
// <button>Click here!</button>
```


## :bulb: Tidbits

You can use the `instanceof` keyword to know if an object is an instance of a certain class.

```js
class Person {
  // ...
}

const person = new Person();

console.log(person instanceof Person)   // true
```

### Errata :bug:

I referred to `constructor` as a _keyword_ in the video. That's a mistake. Keywords (like `if`, `for`, `function`, `class`) are not valid identifiers (i.e. `let class = "Web";` raises an error). You can find a list of JavaScript keywords in the [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords).


### JavaScript is _not_ class-based

A little technical thing here. JavaScript doesn't use the regular class-based object-oriented programming (OOP) like in Java and C++. It uses a _prototype-based_ OOP instead.

We don't have to worry much about it though. But be aware that lowkey, what appears to be a class declaration:

```js
class Person {
  constructor(name, email, phone, status) {
    this.name = name;
    this.email = email;
    this.phoneNumber = phone;
    this.status = status;
  }

  greet() {
    console.log(`Hello from ${this.name}`);
  }
}
```

is actually quite this in JavaScript:

```js
// Constructor (declare instance properties here)
function Person(name, email, phone, status) {
  this.name = name;
  this.email = email;
  this.phoneNumber = phone;
  this.status = status;
}

// Prototype (add methods here)
Person.prototype.greet = function() {
  console.log(`Hello from ${this.name}`);
}
```

In fact, the `class` syntax is said to be a _syntactic sugar_.
