function wait(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      console.log("done");
      resolve();
    }, ms);
  });
}

console.log("Start");
const promise = wait(1000);
console.log(promise);

// setTimeout(function () {
//   console.log(promise)
// }, 1000)