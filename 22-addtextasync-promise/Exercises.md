# Exercises for 22: `addTextAsync` Promise

No "exercises" actually. Just some notes.

We've seen how to run a certain code after _a_ promise settles:

```js
const mypromise = functionThatReturnsPromise();

mypromise.then((myvalue) => {
  // do something
})

const yourpromise = anotherFunctionThatReturnsPromise();

yourpromise.then((yourvalue) => {
  // do something
})
```

But how do we do something after _both_ promises settle? `Promise.all()` is the answer. It takes an array of promises, say, `[promise1, promise2]`, and it returns a promise. The returned promise resolves after `promise1` and `promise2` have resolved. Also, it resolves with an array of the resolved values of `promise1` and `promise2`, respectively. However, if either of the promises is rejected, the returned promise rejects. It rejects with the rejected value of the (first) rejected promise.

```js
const mypromise = functionThatReturnsPromise();
const yourpromise = anotherFunctionThatReturnsPromise();

const ourpromise = Promise.all([mypromise, yourpromise])
ourpromise
  .then(([myvalue, yourvalue]) => {
    // do something after both promises resolve
  }).catch(error => {
    // handle error
  });
```

Checkout the [MDN documentation on `Promise.all`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all) to learn more about it.