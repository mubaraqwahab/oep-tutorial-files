// States:
// 1. pending
// 2. fulfilled
// 3. rejected

const promise = new Promise((resolve, reject) => {
  // async code
  // resolve(2 + 3)
  reject(new Error("failed"))
});

console.log(promise)