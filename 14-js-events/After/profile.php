<?php

session_start();

// Make sure user is logged in before coming here
if (!isset($_SESSION["user"]) || isset($_POST["logout"])) {
  session_destroy();
  header("Location: login.php");
  exit;
}

$user = $_SESSION["user"];

?>

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile | Simple Website</title>
    <link rel="stylesheet" href="style.css" />
  </head>

  <body>
    <?php include("_nav.php") ?>

    <h1>Profile</h1>

    <p>Name: <?= $user["name"] ?></p>
    <p>Email: <?= $user["email"] ?></p>
    <p>Following: <?= count($user["following"]) ?></p>
    <p>Followers: <?= count($user["followers"]) ?></p>

    <form>
      <input type="search" name="query" placeholder="Type here to find users...">
      <input type="submit" name="submit" value="Search">
    </form>

    <ul>
      <?php
        // Find users whose names match the query
        if (isset($_GET["submit"])) {
          require('users.php');

          foreach (get_name_matches($_GET["query"]) as $name) {
            echo "<li>$name</li>";
          }
          unset($name);
        }
        ?>
    </ul>

    <form method="POST">
      <input type="submit" name="logout" value="Logout">
    </form>

    <script src="find-matches.js"></script>
  </body>

</html>