// Get the search input element
const searchInput = document.querySelector('input[name="query"]');

function handleInput(event) {
  // console.dir(searchInput.value)

  console.log(event.target.value)
}

// Add a listener to the input event
// searchInput.addEventListener("input", handleInput)

// Another way of doing it
searchInput.oninput = handleInput;

