// This file contains the "database" and some utility functions.

// The users database.
const users = {
  esatyaman: {
    displayName: "Esat Yaman",
    password: "Esat123",
  },
  sadiqyelwa: {
    displayName: "Sadiq Yelwa",
    password: "sadiq.y",
  },
  maryammoh: {
    displayName: "Maryam Mohammad",
    password: "maryam_",
  },
  stephshyayet: {
    displayName: "Stephan Shyayet",
    password: "steph01",
  },
  mubwahab: {
    displayName: "Mubaraq Wahab",
    password: "mubwa",
  },
};

/** Return true if the credentials are valid. Return false otherwise. */
export function verifyCredentials(username, password) {
  return users[username]?.password === password;
}

/**
 * Return the display name of username.
 * If username doesn't exist, return undefined.
*/
export function getUserDisplayName(username) {
  return users[username]?.displayName;
}
