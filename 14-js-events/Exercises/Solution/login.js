import { verifyCredentials, getUserDisplayName } from "../utils.js";

const form = document.querySelector("form");

function handleFormSubmit(event) {
  // Prevent the browser from reloading the page.
  event.preventDefault();

  // Get the username and password typed in the form
  const username = event.target.elements.username.value;
  const password = event.target.elements.password.value;

  // Get a reference to the #form-message paragraph (check login.html).
  const msgBox = document.getElementById("form-message");

  // If the credentials are valid
  if (verifyCredentials(username, password)) {
    msgBox.textContent = `Welcome ${getUserDisplayName(username)}!`;
    msgBox.className = "alert-success";
  }
  // Otherwise, i.e. if the credentials are invalid
  else {
    msgBox.textContent = `Invalid credentials!`;
    msgBox.className = "alert-error";
  }
}

form.addEventListener("submit", handleFormSubmit);
