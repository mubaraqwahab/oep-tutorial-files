# Exercises for 14: Events

I encourage you to try these out after watching the video.
In case something's not clear, or you get stuck, feel free to ask me.
All the best!

## First, a tip: `preventDefault`

Submitting a form reloads the page by default (or redirects to a different page if the `action` attribute is specified).

You may not want this sometimes, especially when you're using JavaScript to handle form submission.

Event objects have a `preventDefault` method that you can call to prevent the default action. Here's an example:

```js
form.addEventListener("submit", function(event) {
  event.preventDefault();
  // Handle form submission yourself.
});
```

## Exercise: Client-Side Authentication

You're going to do just what the [previous tip](#first-a-tip-preventdefault) mentions &ndash; handling form submission in JS!

You'll be working with three files (in the same folder as this README), `login.html`, `login.js` and `utils.js`. The first two expecially.

I think the files are self-explanatory. `login.html` contains the HTML for a login page. `login.js` is where you'll do all your work. I've included a lot of comments there to help. `utils.js` contains the users "database" and some functions.

You should notice that the database is an object looks something like:
```js
const users = {
  // ...
  mubwahab: {
    displayName: "Mubaraq Wahab",
    password: "mubwa",
  },
};
```

Here, "mubwahab" is the username of a user. His display name is "Mubaraq Wahab" and his password is "mubwa".

Also, in `utils.js` are two functions, `verifyCredentials` and `getUserDisplayName`. The comments in the file explain them.

You may notice some new syntaxes as well (in all three files). For this exercise, you don't have to worry much about them.

If you're curious though, google "modules in javascript" and "optional chaining in javascript" :wink:.

Oh, and before you start&mdash;a quick tip&mdash;HTML elements have a `textContent` property that you can use to set the text in them. For example:
```js
p.textContent = "I just changed the <p> element from JS!";
```

. . .

Your result should look like this on login success:

![Login success](./login-success.jpg)

and like this on login failure:

![Login success](./login-error.jpg)

*P.S.* You can find my solution to this exercise in the Solution folder in the same folder as this file. Make sure you attempt the exercise before viewing it! (Challenge yourself!)
<br>

## A final (unrelated) tip :bulb:

[The `<noscript>` element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/noscript). You can use this to tell a browser what to show when it doesn't support JavaScript or has JavaScript disabled.

For example:

```html
<!doctype html>
<html>
<head>
  ...
</head>
<body>
  ...
  <noscript>
    <p>
      Hey there! It seems your browser doesn't support JavaScript, or doesn't have it enabled.
    </p>
  </noscript>
  ...
  <script src="some/javascript/file"></script>
</body>
</html>
```
