// Don't worry about the "import ..." line!
// All you need to know for now is that it gives you access to two functions,
// `verifyCredentials` and `getUserDisplayName` that are defined in utils.js
import { verifyCredentials, getUserDisplayName } from "./utils.js";

// Get a reference to the form.
const form = document.querySelector("form");

function handleFormSubmit(event) {
  // Prevent the browser from reloading the page.

  // Get the username and password typed in the form
  // (you may need to get references to the username and password inputs first).

  // Get a reference to the #form-message paragraph (check login.html).
  // In this paragraph, we'll display a message
  // indicating whether log in was successful or not.

  // Verify the username and password (i.e. call verifyCredentials).

  // If the credentials are valid,
  // add some welcome message to the #form-message paragraph.
  // Make sure to include the user's display name in the message (e.g. Hello Mubaraq!).
  // To get the user's display name, call getUserDisplayName with the username.
  // Also change the class of the paragraph to just "alert-success" (for the green colour).

  // Otherwise, i.e. if the credentials are invalid,
  // add some error message to the #form-message paragraph.
  // Also change the class of the paragraph to just "alert-error" (for the red colour).

}

// Add the handleFormSubmit function as a listener to the form's submit event.

// You're done! ;-)