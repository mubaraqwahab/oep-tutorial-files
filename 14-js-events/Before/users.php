<?php

// Get the users from the JSON database
$users = get_database();


// Return content of users database
function get_database() {
  return json_decode(file_get_contents("users.json"), true);
}

// Write the users database
function write_database($users) {
  file_put_contents("users.json", json_encode($users));
}

// Return an array of users' names that contain $query
function get_name_matches($query) {
  global $users;
  $matches = [];
  foreach ($users as ["name" => $name]) {
    if (stripos($name, $query) !== false) {
      $matches[] = $name;
    }
  }
  unset($name);

  return $matches;
}

// Save changes to the external JSON database
// and reload the internal copy.
function save_changes_to_db() {
  global $users;

  // Rewrite the database
  write_database($users);

  // Reload the users array
  $users = get_database();
}

?>