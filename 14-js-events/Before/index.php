<?php

session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Simple Website</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php include("_nav.php") ?>

    <h1>Welcome</h1>
    <p>This is a simple HTML website.</p>

    <h2>Languages for Web Dev</h2>
    <ul>
      <li>HTML</li>
      <li>CSS</li>
      <li>JavaScript</li>
    </ul>
  </body>
</html>
