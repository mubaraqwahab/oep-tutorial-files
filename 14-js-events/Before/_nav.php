<nav>
  <ul>
    <li><a href="index.php">Home</a></li>
    <li><a href="about.php">About</a></li>
    <?php
    if (!isset($_SESSION["user"])) {
      echo '<li><a href="login.php">Log in</a></li>';
    } else {
      echo '<li><a href="profile.php">Profile</a></li>';
    }
    ?>
  </ul>
</nav>