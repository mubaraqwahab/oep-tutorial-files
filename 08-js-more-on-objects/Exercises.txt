EXERCISES FOR 08: MORE ON OBJECTS

I encourage you to try these out after watching the video.
In case something's not clear, or you get stuck, feel free to ask me.
All the best!



Exercise 1:

There's this concept of events and event handlers in JavaScript that we'll get to see later on, God willing. You can think of an event handler as a function that gets called whenever an "event" occurs. For example, we could have an input element in our HTML file, and set up a handler in JS for the input "change" event, so that we get notified whenever the value of the input changes (e.g. when the user types into it).

Consider this event object that we get when the user types "hey there" into the input element.

  {
    bubbles: true
    defaultPrevented: false
    target: {
      className: "form-input form-input-large",
      id: "",
      name: "query",
      placeholder: "Type something here",
      required: false,
      type: "text"
      value: "hey there",
      // ... lots of other props and methods
    },
    type: "change",
    // ... a few other props
  }

The "target" property refers to the input element that raised the event. We see from the props in the "target" property that the input element must have looked something like:

  <input class="form-input form-input-large" type="text" name="query" placeholder="Type something here">

What interests us however is what the user typed (the "value" prop of the "target" prop).
Use object destructuring to get just the "value" property of the "target" property from the event object.




Exercise 2:

I indicated in the video that Object.assign() method performs a shallow copy (See footnote 1 at the end of this file). This exercise (adapted from the https://justjavascript.com course which I encourage you to follow) demonstrates it.

If we have an object like so:

  const sadiq = {
    surname: 'Yelwa',
    email: "sadiq@yelwa.com"
  }

Then copying it like this should be fine for us:

  const mubaraq = Object.assign({}, sadiq)

And then we could change values as we wish:

  mubaraq.surname = "Wahab";
  mubaraq.email = "mub@wahab.com"

Running these should then give us two different results

  console.log(sadiq);
  console.log(mubaraq);

Now let's see what happens when our object contains other objects (like plain objects or arrays), like so:

  const esat = {
    surname: 'Yaman',
    email: "esat@yaman.com",
    address: { city: 'Istanbul' }  // A "sub-object" if you like
  }

Let's shallow copy it:

  const maryam = Object.assign({}, esat);

And change values in the new result:

  maryam.surname = "Mohammad"
  maryam.email = "maryam@moh.com"
  maryam.address.city = "Abuja"

Now run this and observe the result:

  console.log(esat);
  console.log(maryam);

Esat's city should be "Abuja" 😲. In simple terms, Object.assign() wouldn't make copies of sub-objects. So, esat.address and maryam.address would refer to the same object. Watch out on that.




Exercise 3 (Array destructuring)

React has some special functions called "hooks". One of them is called "useState" and is used to manage state in a website (like the theme preference (See footnote 2)). Also, it's a function that returns an array. Let's *assume* this is what useState looks like internally:


  function useState(initialState) {
    let state = initialState;

    const setState = function(newState) {
      state = newState;
      console.log("New state set!", state);
    }

    return [ state, setState ];
  }

You don't need to worry about what it does. All that matters to us here is what it returns - an array containing a value (could be any type) and function.

Call useState and pass any argument into it (a number, object - anything) and destructure the return value, so that you have two variables, for example, a `theme` variable and a `setTheme` function:

  const result = useState("light"); // You may pass any value here
  const theme = result[0];  // The variable name could be anything
  const setTheme = result[1];  // The variable name could also be anything

Of course, you shouldn't write yours this way. Use the array destructuring syntax to reduce the three statements to one.

You can test your results like so:

  console.log(theme)  // This should be "light", i.e. the value you passed to useState()
  console.log(setTheme("dark"))  // The output should be "New state set! dark"



💡 Tidbit: We'll learn how to create object templates (i.e. classes) in a later video, God willing. They'll help us avoid worrying about shallowly copying objects.



Footnotes:
1. Technically, nothing gets copied. Object.assign() only performs assignments. It does something more like this:

  const original = {
    name: "My Name",
    description: "Some quite long description",
    sayHi() {
      console.log("Hello there!");
    }
  }

  const copy = {};

  // Start copying
  for (property in original) {
    copy[property] = original[property];
  }
  // End copying

  console.log(copy)

Object.assign() basically writes properties into the target object from the source object(s).

2. From what I've seen, useState() is usually not used for managing an app-wide state like the theme preference. Rather, it's used for simpler things like "isNavOpen" or "isLoading". Anyway, ...