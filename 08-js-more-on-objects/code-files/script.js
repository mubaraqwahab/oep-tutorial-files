// OBJECT DESTRUCTURING

// const person = {
//   name: "Mubaraq",
//   email: "mub@gmail.com",
//   phoneNumber: "+1234567890",
//   status: "student"
// }

function sendTextMessage({ name = "Recipient", phoneNumber: phone }, message) {
  // let name = recipient.name;
  // let phoneNumber = recipient.phoneNumber;

  // let { name = "Recipient", phoneNumber: phone } = recipient;

  // Code ....

  console.log(`Sent to ${name} (${phone}) successfully`);

}

// sendTextMessage(person, "Hey there!");


// ARRAY DESTRUCTURING

// let arr = [20, 30, 50, 70]

// let [ , second,, fourth ] = arr;

// console.log(second, fourth)

// OBJECT EQUALITY*


const person = {
  name: {
    first: "Mubaraq",
    last: "Wahab"
  },
  email: "mub@gmail.com",
  phoneNumber: "+1234567890",
  status: "student"
}

const person1 = {
  name: "Mubaraq",
  email: "mub@gmail.com",
  phoneNumber: "+1234567890",
  status: "student"
}

// const person2 = person;

// console.log(person1 === person2)

// COPYING OBJECTS


const person2 = Object.assign({}, person);

console.log(person2)

person2.name.first = "Ibrahim"

console.log("Person", person)


