const people = [
  ["Esat", "Yaman", "esat@yaman.com", "Male"],
  ["Sadiq", "Yelwa", "sadiq@yelwa.com", "Male"],
  ["Maryam", "Mohammad", "maryam@moh.com", "Female"],
  ["Stephan", "Shyayet", "steph@shyayet.com", "Male"],
  ["Mubaraq", "Wahab", "mubaraq@wahab.com", "Male"]
];

// OBJECT LITERAL

// const person = {
//   firstName: "Esat",
//   lastName: "Yaman",
//   email: "esat@yaman.com",
//   gender: "Male"
// };

// console.log(person)

// DOT NOTATION

// console.log(`His first name is ${person.firstName}`);

// person.email = "esat2@yaman.com";


// person.level = 300;

// console.log(person)

// BRACKET NOTATION

// console.log(`His first name is ${person["firstName"]}`);

// person["email"] = "esat2@yaman.com";


// person["level"] = 300;

// console.log(person)

// const person1 = {
//   name: {
//     first: "Esat",
//     last: "Yaman"
//   },
//   hobbies: [""]
// }

// console.log(person1.name.first)


// METHODS

// const person = {
//   name: "Maryam",
//   dept: "CS",
//   changeDept: function(newDept) {
//     this.dept = newDept;
//   },
// }

// console.log(person.dept)

// person.changeDept("Software eng")

// console.log(person.dept)


// COMPUTED PROPERTIES

// let prop = prompt("Type a property here to create it"); //email
// let value = prompt("Type its value");

// const person = {
//   name: "Maryam",
//   dept: "CS",
//   changeDept: function(newDept) {
//     this.dept = newDept;
//   },
//   [prop + " address"]: value,
// }

// console.log(person)

// console.log(person["email address"])


// SHORTHAND SYNTAX

// const person = {
//   name: "Maryam",
//   dept: "CS",
//   changeDept(newDept) {
//     this.dept = newDept;
//   },
// }

// console.log(person.dept)

// person.changeDept("Software eng")

// console.log(person.dept)

let name = "Sadiq"
let level = 300


const student = { name, level }

student.name = 'Yelwa'

console.log(student)

student = { name: "Sadiq" }