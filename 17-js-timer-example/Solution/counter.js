const count = document.getElementById("count");
const startBtn = document.getElementById("startBtn");
const stopBtn = document.getElementById("stopBtn");

let seconds = 0;
let intervalId = null;

startBtn.addEventListener("click", function () {
  // Get the number (in seconds) typed
  seconds = Number(count.value);

  // Don't proceed if there's an interval already
  // or if seconds equals zero
  if (intervalId !== null || seconds === 0) return;

  // Disable the count input and start button
  count.disabled = true;
  startBtn.disabled = true;

  intervalId = setInterval(function () {
    seconds--;
    // Update the UI
    count.value = seconds;

    // Reset the counter when seconds reaches zero
    if (seconds === 0) {
      resetCounter();
      alert("Done!");
      return;
    }
  }, 1000);
});

stopBtn.addEventListener("click", function () {
  if (intervalId === null) return;

  resetCounter();
})

function resetCounter() {
  clearInterval(intervalId);
  intervalId = null;
  count.value = seconds = 0
  // Enable the count input and start button
  count.disabled = false;
  startBtn.disabled = false;
}