const time = document.getElementById("time");
const startBtn = document.getElementById("startBtn");
const stopBtn = document.getElementById("stopBtn");

let seconds = 0;
let intervalId = null;

startBtn.addEventListener("click", function () {
  if (intervalId !== null) return;

  intervalId = setInterval(function () {
    seconds++;
    time.textContent = seconds;
  }, 1000);
});

stopBtn.addEventListener("click", function () {
  clearInterval(intervalId);
  time.textContent = seconds = 0;
  intervalId = null;
});
