# Exercise for 17: A Timer Example

Create a countdown that works in a manner similar to the following:

![Countdown demo](countdown.gif)

Notice that
* the countdown stops automatically, when it reaches 0.
* the input and start button are disabled while counting down. (See the tip below.)

You can use the HTML file from the video as a starting point. (You may need to edit it though.)
I suggest you use a new javascript file, and not just edit the one from the video.

All the best!

*P.S.* In case you get stuck, you'll find my solution in the `Solution` folder in the same directory as this file. Make sure you try the exercise before checking it though :wink:.

## :bulb: Element attributes

You can get and set the HTML attributes of elements through JS. We've seen an example before:

```js
function handleInput(event) {
  console.dir(searchInput.value)
}
```

This gets the HTML `value` attribute of the `searchInput`.

You can get (and set) other attributes similarly:

```js
// Assume `input` refers to an input element
input.disabled = true
console.log(input.className) // Note: `class` attribute becomes className
```