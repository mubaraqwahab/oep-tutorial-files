# Exercises for 13: Getting DOM Elements

I'm trying to conserve time because of exams, so I won't always give exercises during this time (It takes some serious time to come up with them 😅).

However, I'll try to share one or two tidbits. Like I have below.

## :bulb: Styling with JavaScript

Consider this HTML document. You can use it to test what follows. (See `test.html` in the same folder as this file.)

```html
<!doctype html>
<html>
  <head>
    <title>Exercise 13</title>
    <style>
      .bg-dark { background-color: rgb(50,50,50); }
      .bg-light { background-color: rgb(230,230,230); }
      .text-light { color: white; }
      .text-blue { color: blue; }
    </style>
  </head>
  <body>
    <h1>Styling with JavaScript!</h1>
  </body>
</html>
```

You can apply styles to HTML elements from JavaScript. There are a number of ways to do that. Here are some:

```js
const h1 = document.querySelector("h1");
h1.classList.add("bg-dark", "text-light")
// Notice I didn't do `add("bg-dark text-light")`
```

To remove a class.

```js
h1.classList.remove("text-light");
```

You can toggle classes as well.

```js
h1.classList.toggle("bg-dark");
```

You can also modify the classes using the `className` property like so:

```js
// Unlike classList, you'd manipulate this like you do with strings
h1.className += " text-blue bg-light"
// Notice the space before `text-blue...`
```

It feels a bit constrained though.

You can apply styles directly too, if you will.

```js
h1.style.textTransform = "uppercase";
// Notice how we write the CSS property
// `text-transform` in camel case in JavaScript
```

That'll be it for this episode. Bye :wave:.

## :bulb: Update

I realised later that `setAttribute` and `getAttribute` work quite differently than I had mentioned in the video.

I found [a question on this issue](https://stackoverflow.com/q/29929797/12695621) on Stack Overflow. Also I learnt elsewhere that [_element property_ and _element attribute_ may not be the same.](https://stackoverflow.com/a/6004028/12695621)

Thus do, for example,

```js
// Set property
element.placeholder = "...";
// Get property
const placeholder = element.placeholder;
```

and not

```js
// Set attribute (may not work as expected)
element.setAttribute("placeholder", "...");
// Get attribute (may not work as expected)
const placeholder = element.getAttribute("placeholder");
```

unless, of course, you know what you're doing :smile:.

Bye :wave: