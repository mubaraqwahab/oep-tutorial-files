<?php

session_start();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>About | Simple Website</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php include("_nav.php") ?>

    <h1>About</h1>
    <p>Get to know us.</p>

    <p>
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Soluta ut
      mollitia sapiente, delectus ipsam cum perspiciatis impedit aperiam
      aliquam! Non pariatur tempora aut a voluptatum excepturi illo, explicabo
      debitis harum.
    </p>
  </body>
</html>
