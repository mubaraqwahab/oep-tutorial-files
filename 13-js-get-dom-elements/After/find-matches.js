// Get all "input" elements, and index it to get just the first
console.log(document.getElementsByTagName("input")[0])

// Get the first element with the given id
console.log(document.getElementById("query"));

// Get all elements that have a certain class
console.log(document.getElementsByClassName("hey"))

// Get the first element that matches the CSS selector
const searchInput = document.querySelector('input[name="query"]');

// Get all elements that match the CSS selector
console.log(document.querySelectorAll('input'))

// One way to set an attribute
searchInput.value = "Mubaraq"

// Another way to set an attribute
searchInput.setAttribute("placeholder", "Type");

// Get an attribute
console.log(searchInput.getAttribute("placeholder"))

// You can get attributes like this too
console.dir(searchInput.value)

