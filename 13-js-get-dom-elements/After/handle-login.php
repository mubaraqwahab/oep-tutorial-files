<?php

session_start();

require('users.php');

if (isset($_POST['submit'])) {
  // Find the user with the given credentials
  foreach ($users as $user) {
    if (
      $user["email"] === $_POST["email"]
      && $user["password"] === $_POST["password"]
    ) {
      // Credentials are valid
      $_SESSION["user"] = $user;
      header("Location: profile.php");
      exit;
    }
  }
  unset($user);
}

session_destroy();
header("Location: login.php");