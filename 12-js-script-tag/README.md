# Note!

In the next few videos, we would be using some JS features that require loading our HTML files from a server (i.e. not from the file system). We would also need to view some PHP files, so make sure you still have XAMPP installed.

_(Optional)_ You must have noticed that I've been using a live server in the videos to reload the browser automatically. Here are some tools that can help you set it up (I haven't tried some of them though):

* [Live Server for VS Code](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwiY2cK1obPpAhVYRhUIHXrbABAQFjAAegQIARAB&url=https%3A%2F%2Fmarketplace.visualstudio.com%2Fitems%3FitemName%3Dritwickdey.LiveServer&usg=AOvVaw3kYWeDmI3de_HNoQCeEpFe).
* [LiveReload for Sublime Text](https://medium.com/@theyesyed/how-to-live-reload-in-sublime-text-3-with-auto-saving-features-51e06f681ab1).
* [Live Server](https://pypi.org/project/live-server/) (<abbr title="Command-line interface">CLI</abbr>).

If you need live reload for files served from XAMPP
* [Live Server Web Extension for Chrome](https://chrome.google.com/webstore/detail/live-server-web-extension/fiegdmejfepffgpnejdinekhfieaogmj/) (requires Live Server for VS Code).
* [Live Server Web Extension for Firefox](https://addons.mozilla.org/en-US/firefox/addon/live-server-web-extension/) (also requires Live Server for VS Code).

Of course, you can find more on the internet.