console.log("Start");

function addTextAsync(text) {
  return new Promise((resolve, reject) => {
    const para = document.getElementById("para");

    if (!para) {
      reject(new Error("para doesn't exist"));
      return;
    }

    wait(2000).then(() => {
      para.textContent += text;
      resolve();
    })
  })
}

function addedText() {
  console.log("Text added!");
}

function addTextFailed() {
  console.log("Failed to add text!");
}

function wait(ms) {
  return new Promise((resolve, reject) => {
    if (ms < 0) {
      return reject(new Error("time value invalid"));
    }
    setTimeout(resolve, ms);
  });
}

// wait(2000)
//   .then(() => addTextAsync("Some random text."))
//   .then(addedText, addTextFailed);

async function runAddText() {
  try {
    await wait(2000);
    await addTextAsync("Some random text")
    addedText();
  } catch (error) {
    addTextFailed();
  }
}

runAddText();