function getRequest(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url)

    xhr.onload = function () {
      if (xhr.status === 200) {
        resolve(xhr.response);
      } else {
        reject(xhr.status);
      }
    }

    xhr.onerror = function () {
      reject(xhr.status);
    }

    xhr.send();
  })
}

getRequest("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => console.log(response))
  .catch(status => {
    if (status === 0) {
      console.log("Request failed");
    } else {
      console.log(`Server responded with code ${status}`);
    }
  });

// SOLUTION

(async function () {
  try {
    const response = await getRequest("https://jsonplaceholder.typicode.com/todos/-1");
    console.log(response)
  } catch (status) {
    if (status === 0) {
      console.log("Request failed");
    } else {
      console.log(`Server responded with code ${status}`);
    }
  }
})();