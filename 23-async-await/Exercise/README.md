# Exercise 23

## GET Request

One way to make a network request from the browser is using an `XMLHttpRequest` object (XHR for short). This way uses asynchronous callbacks.

Let's use it to get the content of the page at https://example.com. But, take a deep breath first :smile:. Now skim over the following code:

```js
// Create a new XHR object
const xhr = new XMLHttpRequest();

// Initialize the request
xhr.open("GET", "https://example.com");

// Add a listener for the "load" event.
// The load event fires when the XHR completes.
xhr.onload = function () {
  if (xhr.status === 200) {
    console.log(xhr.response);
  } else {
    console.log(`Server responded with code ${xhr.status}`);
  }
}

// Add a listener for the "error" event.
// The error event fires when the XHR fails.
xhr.onerror = function () {
  console.log("Request failed");
}

// Send the request
xhr.send();
```

Of course, copy it into your browser console and run it.

> :warning: You may get an error if you open a new browser tab and try to run it (because of browser policies). You can visit a website like example.com and try to run it from the console there, or, start a local server and run it from the console there. (The latter is a better choice.)

You should get output similar to:

```
<!doctype html>
<html>
<head>
    <title>Example Domain</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
      ... (clipped lines)
    </style>
</head>

<body>
<div>
    <h1>Example Domain</h1>
    <p>This domain is for use in illustrative examples in documents. You may use this
    domain in literature without prior coordination or asking for permission.</p>
    <p><a href="https://www.iana.org/domains/example">More information...</a></p>
</div>
</body>
</html>
```

Change the "https://example.com" URL, if you wish, and run again. You can also try using an invalid URL or a page that doesn't exist.

Stay calm if it looks a bit complicated! Don't worry, there's another Web API[^1] that simplifies sending requests.

My goal here is actually twofold; first, that you're aware that there's something called an "XML HTTP Request", and second, for you to practice changing the `promise.then()` syntax to async/await.

Now, let's wrap the code above in a promise. (Credit to ["Promisify & simplify XHR"](https://gist.github.com/l4sh/cf407321ad56ba5d4c44edde4ba53ffc) for the following code)

```js
function getRequest(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url)

    xhr.onload = function () {
      if (xhr.status === 200) {
        resolve(xhr.response);
      } else {
        reject(xhr.status);
      }
    }

    xhr.onerror = function () {
      reject(xhr.status);
    }

    xhr.send();
  })
}
```

The function `getRequest` takes a URL and returns a promise wrapping an XHR. The promise resolves with the content of the "resource" at the URL if the request succeeds. Otherwise, it rejects with a status code. (Don't mind the jargon here. In a later video, God willing, I'll discuss these things).

So, how do we use the function? Well ...

```js
getRequest("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => console.log(response))
  .catch(status => {
    if (status === 0) {
      console.log("Request failed");
    } else {
      console.log(`Server responded with code ${status}`);
    }
  });
```

[JSONPlaceholder](https://jsonplaceholder.typicode.com) is just a website for testing network requests if you will.

Now. The actual exercise.

Convert the above snippet to async/await sytax.

There's a `getRequest.js` file in the same folder as this README containing the code you need.

You'll find my solution in the `Solution` folder. Of course, you should only view it when you're done or when you get stuck :wink:.

All the best!



## :bulb: A tip before you leave

You may hear of an "Immediately-Invoked Function Expression" (or IIFE).

You may need a function that you're going to call immediately (and only need once), and &mdash; because naming things is hard &mdash; you don't know what to call it.

This works:

```js
function doSomething() {
  console.log("Doing something...");
}

// You only need to call this once
doSomething()
```

and so does the IIFE:

```js
// Notice no need for function name
(function () {
  console.log("Doing something...");
})();
```

Bye :wave:

**Footnotes**
[^1]: Web APIs are the "things" (objects, functions, ...) that don't belong to the core JavaScript, but browsers provide us with. E.g. the DOM, setTimeout, XHR, ....