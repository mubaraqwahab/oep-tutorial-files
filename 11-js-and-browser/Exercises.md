# 11: JavaScript and the Browser

No exercises for this video 🙂. Just a few tips :bulb:.

## Global object or built-in object? :thinking:

I referred to the `window` object as **"the" global object** in a browser environment. Also I mentioned that the **built-in objects** are properties of the global object. You may find the built-in objects referred to as _standard built-in objects_, or _global objects_ in some texts (like the [MDN reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)).

## Navigating programmatically

You can navigate to a URL from JavaScript using the `window.location` object.

Open any HTML file (or website) in your browser. Then open the browser console and run `window.location`. You should get an object with properties identifying the location of the current page (like the URL).

You can get the URL specifically by running `window.location.href`. As well, you can set the URL. Try:

```js
window.location.href = "https://gitlab.com"
// OR:
// location.href = "https://gitlab.com"
```

Your browser should take you to GitLab. Running `window.location = "https://gitlab.com"` has a similar effect.

Try experimenting with other properties of `window.location`.

## Browser storage

Browsers provide some [methods for _persisting_ data](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Client-side_storage) offline. The simplest ones are _local storage_ and _session storage_. I encourage you to read about them.

## Node.js

You might have heard of [Node.js](https://nodejs.org) (or Node). It's (I guess the most) common JavaScript runtime outside of browsers. It allows you to, for example, run JavaScript code from a command line or even a server. We won't be _too_ concerned about it however.


## 📌 Remember!

The `window` object exists in a browser context. That means `document`, `location`, `localStorage` and other properties **may not** be available outside of a browser (e.g. Node.js).
