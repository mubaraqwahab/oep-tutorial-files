// const names = [];

// Get name function
// const getName = () => prompt("Enter a name (type 'stop' to stop)");


// WHILE LOOP

// let name = getName();

// while (name !== "stop") {
//   names.push(name);
//   name = getName();
// }


// DO WHILE LOOP

// let name;

// do {
//   name = getName();

//   if (name === "stop") {
//     break;
//   }

//   names.push(name);
// } while (true);

// console.log("The names« are", names);


const names = [ "Maryam", "Isa", "Stephan", "Esat", "Sadiq" ];

// FOR LOOP

// for (let i = 0; i < names.length; i++) {
//   if (i === 1) {
//     continue;
//   }
//   console.log(`Name at index ${i} is ${names[i]}`);
// }


// FOR .. OF LOOP

// for (let name of names) {
//   console.log(name);
// }

// names.forEach(name => {
//   console.log(name);
// })


// FOR .. IN LOOP

for (let index in names) {
  console.log(index);
}
