# Exercises for 09: Rest and Spread Syntax

I encourage you to try these out after watching the video.
In case something's not clear, or you get stuck, feel free to ask me.
All the best!

## Exercise 1

JS comes with a bult-in `Math` object that has methods like `sin()` (for computing sine), `sqrt()` (for square root), `log10()` (for logarithm) and others. You can read more about it in the [MDN docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math).

Suppose we have an array of the following ages

```js
const ages = [ 24, 7, 67, 49, 7, 22, 39, 65, 43, 40 ];
```

And we want to find the range of the ages (i.e. the difference between the largest and smallest age). The `Math` object has `max` and `min` methods that would help us find the largest and smallest numbers in a sequence respectively. This is how to use them:

```js
const smallest = Math.min(5, 4, -1, 7)  // Result: -1
const largest = Math.max(5, 4, -1, 7)   // Result: 7
```

However they accept numbers as arguments, and _not_ arrays of numbers as we wish. Write a `range` function that accepts an array parameter and returns the range of the numbers in the array. Make sure to use the rest/spread syntax. You should be able to call the function like so:

```js
range(ages)  // Result: 60
```




## :bulb: Tidbit

You can use object destructuring with the rest syntax, like so:

```js
let obj1 = {
  name: "Mubaraq",
  username: "mubw",
  hobbies: [],
  level: 0
}
let { username, ...rest } = obj1  // The name "rest" is arbitrary

console.log(username) // mubw
console.log(rest)     // { name: "Mubaraq", hobbies: [], level: 0 }
```

You can do similarly with arrays.

```js
let animals = [ "cat", "bird", "fish", "snail" ]
let [ first, second, ...others ] = animals

console.log(first)  // cat
console.log(second) // bird
console.log(others) // [ "bird", "fish", "snail" ]
```