// REST PARAMS

const sum = (initial, ...numbers) => {
  return numbers.reduce((sum, num) => sum + num, initial);
}

// console.log(sum(1,2,3,4,5,6,7));



// SPREAD IN FUNCTION CALLS

let integers = [ -5,-2,0,1,4,2,7,34 ];

// console.log(sum(...integers));




// SPREAD IN ARRAY LITERALS & STRINGS

let even = [2,4,6,8,10]
let odd = [1,3,5,7,9]

let whole = [ ...even, ...odd ];

let myString = "1234567"

let splitString = [ ...myString ]

// console.log(splitString)


// SPREAD IN OBJECT LITERALS

let obj1 = {
  name: "Mubaraq",
  email: "mub@gmail.com"
}

let obj2 = {
  email: "mubaraq@gmail.com",
  level: 300
}

let obj3 = { ...obj1, ...obj2 };

// console.log(obj3)