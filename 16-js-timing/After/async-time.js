console.log("Start");

// // timeouts and intervals
// setTimeout(function () {
//   console.log("It's two seconds already!");
// }, 2000);

// console.log("Third");

// const intervalId = setInterval(function () {
//   console.log("Tick");
// }, 1000)

// setTimeout(function () {
//   clearInterval(intervalId);
// }, 2000)

// clearTimeout()

// Passing parameters
// let n = 5;

// setTimeout(function (n) {
//   console.log(n * n)
// }, 1000, n)

// n = 6;

// Calling from a function

// function f() {
//   console.log("Entering function f");
//   setTimeout(function () {
//     console.log("Time up!");
//   }, 1000);
//   console.log("Exiting function f");
// }

// f();
// console.log("Exited")

console.log("First line");
setTimeout(function () {
  console.log("Second line")
})
console.log("Third line")