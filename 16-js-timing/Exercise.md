# 16: Timing in JavaScript

## Exercise 1

Why does the following code:

```js
console.log("First line");
setTimeout(function () {
  console.log("Second line")
}, 0)
console.log("Third line")
```
produce the following output?

```
First line
Third line
Second line
```

Find out!

> :bulb: There's this concept of an [_event loop_](https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop) associated with asynchronous code. I _strongly_ encourage you to watch this JS conference about the event loop (it's about 27 minutes long): [What the heck is the event loop anyway?](https://youtu.be/8aGhZQkoFbQ). It should help you understand the "funny" behaviour of `setTimeout`.


## :bulb: Callbacks

In the following code, we don't know exactly when the `sendMessage` function will run.

```js
function sendMessage(recipient, message) {
  let sent = false

  // Assume there's some code here to send a message to `recipient`
  // and that `sent` changes to `true` when the message is sent
}

setTimeout(sendMessage, 2000, "Mubaraq", "How are you?");
```

Now suppose there's some action we want to take after trying to send the message (for example, to give the user feedback). We obviously can't do this:

```js
function sendMessage(recipient, message) {
  let sent = false

  // ...
}

function giveFeedback() {
  console.log("Message feedback!");
}

setTimeout(sendMessage, 2000, "Mubaraq", "How are you?");

// This would run before the sendMessage
giveFeedback();
```

So how do we do? Well, one way is to pass a callback function (i.e. `giveFeedback`) to `sendMessage`, so that the function gets called after (trying to) send a message.

```js
function sendMessage(recipient, message, callback) {
  let sent = false

  // ...

  callback();
}

setTimeout(sendMessage, 2000, "Mubaraq", "How are you?", giveFeedback);
```

There's an advantage of flexibility here. We can define `giveFeedback` to take an argument as follows:

```js
function giveFeedback(messageSent) {
  if (messageSent) {
    alert("Your message has been sent!");
  } else {
    alert("Your message could not be sent. Make sure you're connected to the internet.");
  }
}
```

And then we would pass the `sent` variable (in `sendMessage`) to `callback`.

```js
function sendMessage(recipient, message, callback) {
  let sent = false

  // ...

  callback(sent);
}

setTimeout(sendMessage, 2000, "Mubaraq", "How are you?", giveFeedback);
```

Run the code snippets if they're not clear.