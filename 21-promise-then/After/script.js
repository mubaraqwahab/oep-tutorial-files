console.log("Start");

// function addTextAsync(text, onsuccess, onerror) {
//   const para = document.getElementById("para");

//   if (!para) {
//     onerror();
//     return;
//   }

//   para.textContent += text;
//   onsuccess();
// }

// setTimeout(addTextAsync, 2000, "Hello", addedText, addTextFailed);

function addedText() {
  console.log("Text added!");
}

function addTextFailed() {
  console.log("Failed to add text!");
}

// ...

function wait(ms) {
  return new Promise((resolve, reject) => {
    if (ms < 0) {
      return reject(new Error("time value invalid"));
    }
    setTimeout(() => resolve([1, 2, 3]), ms);
  });
}

wait(2000)
  .then(console.log)
  .catch((error) => {
    console.log(error.message);
  });
