console.log("Start");

function addTextAsync(text, onsuccess, onerror) {
  const para = document.getElementById("para");

  if (!para) {
    onerror();
    return;
  }

  para.textContent += text;
  onsuccess();
}

setTimeout(addTextAsync, 2000, "Hello", addedText, addTextFailed);

function addedText() {
  console.log("Text added!");
}

function addTextFailed() {
  console.log("Failed to add text!");
}

// ...

function wait(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      console.log("done");
      resolve();
    }, ms);
  });
}
