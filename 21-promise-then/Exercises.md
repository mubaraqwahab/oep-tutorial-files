# Exercises for 21: Promise ... then

## Exercise 1

Consider this snippet (from [javascript.info](https://javascript.info/promise-basics#tasks)):

```js
let promise = new Promise(function(resolve, reject) {
  resolve(1);

  setTimeout(() => resolve(2), 1000);
});

promise.then(alert);
```

What will the output be? Guess before you run it!

**:bulb: Hint:** A promise can only be fulfilled/rejected once.

## Exercise 2

What about this? What's going to be the output?

```js
let promise = new Promise(function(resolve, reject) {
  setTimeout(() => resolve(2), 1000);
  reject(new Error("failed!"));
});

promise
  .then(console.log)
  .catch(error => console.log(error.message));
```

## Exercise 3

And this?

```js
let promise = new Promise(function(resolve, reject) {
  setTimeout(() => resolve(2), 1000);
  reject(new Error("failed!"));
});

promise
  .then(() => console.log("first fulfill"),
        () => console.log("first reject"))
  .catch(() => console.log("second reject"))
  .then(() => console.log("second fulfill"));
```

## :bulb: A little technical stuff

Guess the output of the following snippet?

```js
console.log("start");

const promise = new Promise((resolve, reject) => {
  console.log("Inside executor");

  resolve("hi")

  console.log("Leaving executor");
});

promise.then(console.log);

console.log("hey")
```

Now run it.

You should get the output:
```
start
Inside executor
Leaving executor
hey
hi
```

Observe the following:

1. The _executor_ function (i.e. the `(resolve, reject) => { ... }` function) is called _synchronously_ &mdash; in fact, it's called when we create the promise.
2. The promise resolves immediately. There's no timeout or something delaying its fulfillment.
3. The function passed to the `.then()` method is called _asynchronously_ i.e. it's not called immediately, even if the promise is already settled.

## :bulb: Final Tip: `new`

There are _some_ "scope-safe constructors" in JavaScript &mdash; i.e., you can use them without the `new` keyword. So, for example, this works:
```js
throw new Error("bla bla");
```
and so does this:
```js
throw Error("bla bla")
```

This doesn't apply to _all_ constructors however.