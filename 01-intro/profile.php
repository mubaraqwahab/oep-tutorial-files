<?php

session_start();

// Make sure user is logged in before coming here
if (!isset($_SESSION["user"]) || isset($_POST["logout"])) {
  session_destroy();
  header("Location: login.html");
  exit;
}

$user = $_SESSION["user"];

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile | Simple Website</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <h1>Profile</h1>

    <p>Name: <?= $user["name"] ?></p>
    <p>Email: <?= $user["email"] ?></p>
    <p>Following: <?= count($user["following"]) ?></p>
    <p>Followers: <?= count($user["followers"]) ?></p>

    <form>
      <input type="search" name="query" placeholder="Type here to find users...">
      <input type="submit" name="submit" value="Search">
    </form>

    <ul>
      <?php
      // Find users whose names match the query
      if (isset($_GET["submit"])) {
        require("users.php");

        foreach ($users as $otherUser) {
          if (stripos($otherUser["name"], $_GET["query"]) !== false) {
            echo "<li>" . $otherUser["name"] . "</li>";
          }
        }
        unset($otherUser);
      }
      ?>
    </ul>

    <form method="POST">
      <input type="submit" name="logout" value="Logout">
    </form>

    <nav>
      <ul>
        <li><a href=".">Home Page</a></li>
        <li><a href="about.html">About Page</a></li>
      </ul>
    </nav>
  </body>
</html>